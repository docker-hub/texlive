FROM adnrv/texlive:minimal

RUN   apt-get update -qq &&\
      apt-get install --no-install-recommends -y \
      fonts-noto-color-emoji \
      libfontconfig \
      && \
      # Clean up
      apt-get autoclean && \
      apt-get autoremove && \
      rm -rf /var/lib/apt/lists/* \
        /tmp/* \
        /var/tmp/* \
      && \
      tlmgr update --self &&\
      tlmgr install \
      # Collections of basic stuff
      collection-basic \
      collection-fontsrecommended \
      collection-latex \
      collection-latexrecommended \
      collection-latexextra \
      collection-langkorean \
      collection-langspanish \
      collection-langportuguese \
      collection-luatex \
      collection-mathscience \
      \
      # Other packages
      biber \
      biblatex \
      biblatex-apa \
      biblatex-ieee \
      courier \
      courier-scaled \
      doublestroke \
      emoji \
      erewhon \
      erewhon-math \
      fira \
      fontawesome \
      fontawesome5 \
      fourier \
      inconsolata \
      ieeetran \
      latexdiff \
      latexmk \
      listofitems \
      logreq \
      ly1 \
      mnsymbol \
      moloch \
      newtx \
      nth \
      realscripts \
      siunitx \
      stix \
      tex-gyre \
      tracklang \
      xcite
