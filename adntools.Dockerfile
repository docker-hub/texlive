FROM adnrv/texlive:tools

# Create the directories
RUN (mkdir -p `kpsewhich -var-value TEXMFLOCAL`/tex/latex || true) &&\
    (mkdir -p `kpsewhich -var-value TEXMFLOCAL`/bibtex/bib || true)

RUN \
    # Get the normal repos
    git clone https://gitlab.com/adin/mcv-academic.git `kpsewhich -var-value TEXMFLOCAL`/tex/latex/mcv-academic &&\
    \
    # Get journal list
    wget https://gitlab.com/adin/journal-list/builds/artifacts/master/download?job=build -O `kpsewhich -var-value TEXMFLOCAL`/bibtex/bib/artifacts.zip &&\
    unzip `kpsewhich -var-value TEXMFLOCAL`/bibtex/bib/artifacts.zip -d `kpsewhich -var-value TEXMFLOCAL`/bibtex/bib/journal-list &&\
    rm `kpsewhich -var-value TEXMFLOCAL`/bibtex/bib/artifacts.zip &&\
    \
    # Get the adn-latex repos
    git clone https://gitlab.com/adn-latex/adn-latex.git `kpsewhich -var-value TEXMFLOCAL`/tex/latex/adn-latex &&\
    git clone https://gitlab.com/adn-latex/codetools.git `kpsewhich -var-value TEXMFLOCAL`/tex/latex/codetools &&\
    git clone https://gitlab.com/adn-latex/adnlogos.git `kpsewhich -var-value TEXMFLOCAL`/tex/latex/adnlogos &&\
    git clone https://gitlab.com/adn-latex/adnbeamertheme.git `kpsewhich -var-value TEXMFLOCAL`/tex/latex/adnbeamertheme &&\
    git clone https://gitlab.com/adn-latex/adnarticle.git `kpsewhich -var-value TEXMFLOCAL`/tex/latex/adnarticle &&\
    git clone https://gitlab.com/adn-latex/beamertools.git `kpsewhich -var-value TEXMFLOCAL`/tex/latex/beamertools &&\
    git clone https://gitlab.com/adn-latex/adnexam.git `kpsewhich -var-value TEXMFLOCAL`/tex/latex/adnexam &&\
    git clone https://gitlab.com/adn-latex/adnmemo.git `kpsewhich -var-value TEXMFLOCAL`/tex/latex/adnmemo &&\
    git clone https://gitlab.com/adn-latex/adnreport.git `kpsewhich -var-value TEXMFLOCAL`/tex/latex/adnreport &&\
    git clone https://gitlab.com/adn-latex/adnlanguages.git `kpsewhich -var-value TEXMFLOCAL`/tex/latex/adnlanguages &&\
    git clone https://gitlab.com/adn-latex/adnsyllabus.git `kpsewhich -var-value TEXMFLOCAL`/tex/latex/adnsyllabus &&\
    \
    # Hash to be sure to find our stuff
    texhash &&\
    \
    # Clean
    apt-get autoclean &&\
    apt-get autoremove &&\
    rm -rf /var/lib/apt/lists/* \
           /tmp/* \
           /var/tmp/*
