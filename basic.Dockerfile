FROM adnrv/texlive:minimal

# set up packages
RUN tlmgr update --self &&\
    tlmgr install scheme-basic